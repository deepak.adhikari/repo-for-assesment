#! /bin/bash

file=$1

echo "checking for file $file "
if [ -e $file ]; then
	echo "$file already exists"
else
	echo "creating file $file"
	touch $file
fi
